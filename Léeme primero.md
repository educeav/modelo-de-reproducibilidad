

=== start-multi-column: ExampleRegion1  
```settings  
number of columns: 2  
border: off  
shadow: off
largest column: left
```

# Léeme
Información importante

# Resumen de ejecución
[[Tablero Kanban]]: 
[[Instrumentación]]
[[Recolección de datos]]
[[Reporte de novedades]]
[[Interferencias]]

# Herramientas
## Tecnologías
- Obsidian + Plug-ins
	- PlantUML
	- Kanban
	- Dataview
- Coggle
- Dropbox
- GitLab
## Lenguajes
- Markdown
- PlantUML
- R

=== end-column ===

# Proceso
El presente proyecto de investigación sigue el siguiente proceso:
```plantuml
scale 1.2
(*) --> "[[[01 Requerimientos|Definición]]]" as p1
--> "[[[02 Diseño|Planificación]]]" as p2
--> "[[[03 Recolección|Recolección]]]" as p3
p3 --> p1
p3 --> p2
p3 --> "4: Análisis"
--> "5: Empaquetamiento"
--> (*)
```

[[01 Requerimientos]]
[[02 Diseño]]
[[03 Recolección]]
[[04 Análisis]]
[[05 Empaquetado]]

=== end-multi-column
