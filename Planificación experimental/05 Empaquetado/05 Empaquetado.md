# 05 Empaquetamiento de resultados
```plantuml
scale 1
(*) --> === F1 ===
=== F1 === --> "Interpretación de resultados" as ir
=== F1 === --> "Identificación de amenazas" as ia
ir --> === F2 ===
ia --> === F2 ===
=== F2 === --> "Interferencias" as in
=== F2 === --> "Identificación de lecciones aprendidas" as ila
in --> === F3 ===
ila --> === F3 ===
--> (*)
```

[[Interpretación de resultados]]
[[Amenazas identificadas]]
[[Interferencias]]
[[Lecciones aprendidas]]