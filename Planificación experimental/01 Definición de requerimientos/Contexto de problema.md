**El contexto del problema se centra en el ámbito de la experimentación reproducible en ingeniería de software.** Basándose en el "modelo problema-solución" ([[PSEM]]) hemos caracterizado las siguientes necesidades del proceso experimental reproducible.  
* Propiciar  un ambiente de cooperación (comunicación)
* Definir qué información es relevante para replicar: resultados, análisis, e interpretaciones.
* Estructurar de forma adecuada la información para poder validar los artefactos de la investigación
* Proveer registros históricos sobre el proceso
* Especificar mecanismos para motivar la replicación de la investigación