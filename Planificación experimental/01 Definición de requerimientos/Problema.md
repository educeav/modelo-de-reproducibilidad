**Replicar un experimento es considerada una tarea que demanda un alto nivel de esfuerzo.** Este esfuerzo se relaciona a una serie de retos que un **investigador** debe considerar desde las etapas tempranas de una investigación "original". Muchos experimentos no pueden ser replicados debido a que carecen de características que faciliten su entendimiento o ejecución; resultando en una escasa cantidad de publicaciones relacionadas con la replicación de investigaciones experimentales. 

Entre los problemas más evidentes son:
- Contar con habilidades en el ámbito experimental para comunicar la información relevante.
- La existencia de conocimiento tácito que rodea la ejecución y la toma de decisiones en la investigación.
- Limitaciones en los procesos de diseminación de hallazgos.
- El seguimiento riguroso de estándares, guías o estructuras enfocadas en el reporte de la investigación.
* La existencia de una gran variedad de contextos o dominios de investigación.
* Pobre gestión de datos.
* Dificultad para recrear arquitecturas tecnológicas, infraestructuras o condiciones de un experimento.