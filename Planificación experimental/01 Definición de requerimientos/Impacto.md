La baja cantidad de publicaciones replicadas impacta directamente a la solidez del [[conocimiento científico]]. El conocimiento solamente puede ser generalizado si las hipótesis son verificadas bajo diferentes contextos. Al no contar con una amplia reproducción de las investigaciones, entonces el conocimiento no alcanza su carácter de "científico".

En la investigación doctoral hemos descrito las cualidades que pueden llegar a ser un factor crítico de la reproducibilidad. En este sentido, una mala práctica de investigación puede afectar la calidad de una investigación, impactando a:
* La [[Auto descripción]] de la investigación
* La capacidad de [[Transferibilidad|transferencia]] de conocimiento relevante
* La posibilidad de incorporar [[Estandarización]] de la ejecución

El impacto negativo puede llegar a ser evidente o no dependiendo de dos situaciones:
- En procesos de investigación interna no es tan evidente debido a que los grupos de investigación suelen establecer esquemas estandarizados o ya cuentan con un conocimiento heredado. Sin embargo, en la investigación externa, esto suele convertirse en un problema.
- En procesos de investigación en dominios o contextos tan particulares donde los investigadores cuentan con mucha experiencia, muchos de los elementos del [[PSEM]] son planteados de forma tácita.