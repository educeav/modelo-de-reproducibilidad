# Proceso de definición de requerimientos

```plantuml
scale 1
(*) --> "[[[Problema|Planteamiento del problema]]]"
--> === F1 ===
=== F1 === --> "[[[Contexto de problema|Definición del contexto]]]" as pc
=== F1 === --> "[[[Objetivos|Definición de objetivos]]]" as po
pc --> === F2 ===
po --> === F2 ===
--> (*)
```
## Entidades importantes
### El ámbito del problema
```plantuml
scale 1.2
class "[[[Problema]]]" as Problema
Problema --> "Contexto": dónde
Problema ->"1..*" Actor: afectados
(Problema, Actor) . Impacto
```
### El ámbito del experimento
```plantuml
scale 1.2
class Experimento
class "[[[Objetivos|Objetivo]]]" as Objetivo
Experimento *-->"*" Objetivo: objetivos
Experimento o--> Contexto: antecedente
Objetivo o--> "Objeto de estudio": qué
Objetivo o--> "Propósito": "por qué"
Objetivo o--> "Perspectiva": quién
Objetivo o-->"*" "Enfoque de calidad": preocupaciones
Contexto o-left->"*" Restricción
```

# Desarrollo
![[Problema]]

![[Contexto de problema]]

![[Objetivos]]

![[Impacto]]