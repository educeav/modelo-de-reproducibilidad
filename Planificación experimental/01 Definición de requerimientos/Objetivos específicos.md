## Objetivos específicos
* Esclarecer los elementos relevantes de la gestión y ejecución del experimento que deben considerarse para la auto descripción de un experimento reproducible 
* Maximizar la comprensión de una investigación experimental a través de la estructuración adecuada del conocimiento y de la información