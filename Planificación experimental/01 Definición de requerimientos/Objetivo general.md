## Objetivo general

Validar la efectividad de un modelo de estrategias de reproducibilidad para obtener un experimento reproducible  a través de la planificación de un experimento por parte de experimentadores no iniciados.

## Estructuración del objetivo
|               Elemento | Valor                        |
| ----------------------:| ---------------------------- |
|           Objeto (Qué) | Planificación de experimento |
|    Propósito (Por qué) | Mejorar la reproducibilidad  |
|    Perspectiva (Quién) | Experimentadores no iniciado |
| Calidad (Preocupación) | Efectividad del modelo       |