---

kanban-plugin: basic

---

## Repositorio

- [ ] #análisis 	ACT012-Procesamiento de resultados


## En definición



## En proceso

- [ ] #análisis [[ACT010-Recolección de instrumentos evaluados]] @{2022-07-30}
- [ ] #analisis [[ACT011-Evaluación de instrumentos de revisión]] @{2022-07-25}


## Completas

- [x] #inducción [[ACT001-Presentación - Procesos de experimentación]]<br>@{2022-06-14}
- [x] #inducción [[ACT002-Repaso - Procesos de experimentación]] @{2022-06-16}
- [x] #inducción  [[ACT003-Tarea - Planificación de experimento]]<br>@{2022-06-17}
- [x] #inducción [[ACT003-Presentación - Reproducibilidad]]<br>@{2022-06-21}
- [x] #inducción [[ACT004-Repaso - Reproducibilidad]]<br>@{2022-06-23}
- [x] #inducción [[ACT005-Retroalimentación - Planificación de experimento y reproducibilidad]]<br>@{2022-06-23}
- [x] #inducción [[ACT006-Taller - Criterios de planificación  y reproducibilidad]]<br>@{2022-06-28}
- [x] #inducción [[ACT007-Discusión - Criterios de planificación y reproducibilidad]]<br>@{2022-06-30}
- [x] #recolección [[ACT008-Ejecución de taller - Grupo A y B]]<br>@{2022-07-05}
- [ ] #análisis [[ACT009-Selección de expertos y envío de instrumentos generados]]<br>@{2022-07-24}




%% kanban:settings
```
{"kanban-plugin":"basic","new-note-template":"Planificación experimental/Anexos/plantillas/Detalle de actividad.md"}
```
%%