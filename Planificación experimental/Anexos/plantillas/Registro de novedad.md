---
tipo_novedad: [incidente, interferencia, lección]
nivel_impacto: 🟥🟧🟨
fecha: YYYY-MM-DD
tipo_amenaza: [constructo, conclusion, interna, externa]
---

### Problema
Describa el problema

### Impacto
- Describir posibles impactos

### Contingencia
- Describir contingencias en caso de haberla 