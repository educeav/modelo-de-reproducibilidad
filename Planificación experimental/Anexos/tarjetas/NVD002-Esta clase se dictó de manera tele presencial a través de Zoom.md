---
tipo_novedad: [incidente]
nivel_impacto: 🟨
fecha: 2022-06-28
tipo_amenaza: [interna]
---

### Problema
Las clases, que estaban orientadas a ser presenciales, tuvieron que darse de forma telepresencial debido a acontecimientos del país (paro nacional).

### Impacto
- Debido a al alto componente teórico y documental, es posible que no se logre captar adecuadamente la atención de los estudiantes afectando el desarrollo del taller.

### Contingencia
- Se realiza presentaciones interactivas que se toman en cuenta como participación en clase.
- Se decide alargar las sesiones de inducción agregando dos clases adicionales:
	- [[ACT006-Taller - Criterios de planificación  y reproducibilidad]]
	- [[ACT007-Discusión - Criterios de planificación y reproducibilidad]]