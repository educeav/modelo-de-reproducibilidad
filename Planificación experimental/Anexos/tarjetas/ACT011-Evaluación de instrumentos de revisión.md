#detalle-actividad

### Actividad
Enviar los documentos recopilados de la [[ACT008-Ejecución de taller - Grupo A y B]] a los [[Experimentadores expertos#Lista de candidatos]] y que realicen la revisión de los documentos.

### Resultados esperados
* Datos numéricos para confirmar la hipótesis

### Locación
Remota

### Material
Para esta etapa se utilizarán dos formularios de recopilación de datos:
- link::[MAT011-Aspectos de una investigación](https://docs.google.com/forms/d/1-o8KvKlF6hVvFtPq2qI85vHDdCbzJ1y76FWQJ2MZy8Q/prefill)  https://forms.gle/eqAbYhXpmnyXdsDp8
- link::[MAT012-Revisión de planificación](https://docs.google.com/forms/d/14gv8IWJ_7QzsB1j8VScU2qZFqrutqJWUyNOi36CkbBw/prefill)  https://forms.gle/X3oxxVkUUqopjktL6


### Participantes
Adicionalmente, cada investigador recibirá dos documentos de acuerdo a la siguiente distribución:

```dataview
table Documentos as "Documentos asignados"
from -"Planificación experimental/Anexos/plantillas"
sort file.name asc
where contains(persona,"experto_participante")
```

### Novedades
- [[NVD005-Un documento no cumple con parámetros]]