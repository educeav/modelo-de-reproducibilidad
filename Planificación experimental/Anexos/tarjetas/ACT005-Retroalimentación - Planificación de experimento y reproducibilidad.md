#detalle-actividad

### Actividad

Tarea individual para revisar lo discutido en [[ACT003-Tarea - Planificación de experimento]] mejorando los detalles del documento de planificación y reproducibilidad.

### Resultados esperados

* Afianzar los criterios aplicados en la planificación de experimentos considerando aspectos de reproducibilidad.

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

1. [[BRT007-Maycol Tituaña Planificación de experimento.docx]]
1. [[BRT008-tarea9.docx]]
1.  [[BRT010-TAREA9_VENEGAS_CAMILA.docx]]
1. [[BRT011-TAREA9-Correccion-Planificacion-Grupo3.docx]]
1. [[BRT012-TAREA9-LOPEZ-CORRECCION-PLANIFICACION-EXPERIMENTO-REPRODUCIBILIDAD.docx]]
1. [[BRT013-TAREA9-Observaciones del experimento-Grupo4-Eduardo Mortensen.docx]]
1. [[BRT014-TAREA9-Revision sobre la planificación-GallardoJuan.docx]]
1. [[BRT015-TAREA9-Revisión sobre la planificación-Otuna Shirley.docx]]
1. [[BRT016-TAREA9-Revisión-Planificación-Experimento-CoronelAndrés.docx]]
1. [[BRT017-TAREA9-Revision-Planificacion-Experimento-Grupo3.pdf]]
1. [[BRT018-Tarea9-Revisión-Planificación-Experimento-MalteMarcelo.docx]]
1. [[BRT019-TAREA9-Revision-Planificación-YepezChristopher.docx]]
1. [[BRT020-TAREA9-RUIZ-JERICO-REVISION-PLANIFICACION.docx]]

### Novedades

- [[NVD001-Estudiantes ausentes en actividades de inducción]]