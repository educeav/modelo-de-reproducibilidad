---
tipo_novedad: [incidente]
nivel_impacto: 🟨
fecha: YYYY-MM-DD
tipo_amenaza: [interna]
---

### Problema
Al momento de acceder a una de las herramientas de soporte proporcionadas, el libro de Género tenía un tamaño muy elevado, lo cual dificultó su descarga por parte del [[Grupo B]]

### Impacto
- El tiempo destinado a la realización de la actividad pudo haberse reducido en comparación con el otro grupo

### Contingencia
- Los estudiantes tubieron tiempo suficiente para completar la tarea por lo que no se tomó ninguna medida de contingencia.