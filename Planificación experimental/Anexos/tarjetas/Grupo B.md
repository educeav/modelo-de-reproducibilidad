### Herramienta a usar
Literatura tradicional base

### Material proporcionado 
- [[MAT007-Presentación grupo B.pdf]]
- Libro: Wohlin - Parte II (pp. 85 - 157)
- Libro: Género - Capítulo 3 (pp. 79 - 109)

### Participantes
- [x] ==Mateo Nicolas Andrade Peñafiel MODELO==
- [x] Jeremy Joel Cadena Diaz
- [x] Michelle Alexandra Cantuña Chicay
- [x] Andres Alejandro Coronel Rodrigues
- [x] Juan Esteban Gallardo Alava
- [x] Andres Isaias Lopez Almeida
- [x] Dennis Marcelo Malte Villarreal