#detalle-actividad

### Actividad

Clase magistral sobre experimentación que tratará los temas:
1. Definición del experimento
2. La importancia y aplicación del experimento en el ámbito del software
3. El proceso del experimento
	1. Requerimientos
	2. Planificación
	3. Recolección de datos
	4. Análisis de datos
	5. Empaquetado de resultados

### Resultados esperados

- Nivelar a los participantes sobre el proceso de planificación de experimentos.

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

- link::[MAT001-Presentación experimento](https://coggle.it/diagram/YqdOlrndxQl8Lc0r/t/experimentos-en-ingenier%C3%ADa-de-software/480f7554f8826df791a4d10e65d8ada33cb8485f3dfbaeb30024496d7f46c826)
- [[MAT002-Planificacion_experimental.pdf]]

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- [[NVD001-Estudiantes ausentes en actividades de inducción]]