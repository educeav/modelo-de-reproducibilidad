---
tag: detalle-actividad
---

### Actividad
Descripción detallada de la actividad

### Resultados esperados
* Resultados que se esperan alcanzar al realizar esta actividad

### Locación
Ubicación física, geográfica o virtual

### Material
- Listado de materiales creados para esta actividad

### Participantes
+ Listado de personas que intervienen en la actividad

### Novedades
+ Listado de novedades o incidentes