#detalle-actividad

### Actividad
Desarrollar una planificación de experimento. Se debe considerar la reproducibilidad. Se explica el uso de materiales complementarios a sus conocimientos para mejorar su planificación. Encuestar a los estudiantes sobre su ejecución.

### Resultados esperados
- Los estudiantes plantean la planificación de un experimento tomando en consideración y lo aprendido en clase (proceso y características de reproducibilidad), utilizando materia adicional para desarrollar un mejor proceso.

### Locación
Presencial, Universidad de las Fuerzas Armadas - ESPE

### Material
![[Grupo A#Material proporcionado]]
![[Grupo B#Material proporcionado]]
- link::[MAT010-Encuesta experimentadores novatos](https://docs.google.com/forms/d/e/1FAIpQLSckPWEbGYKtjfzKwGb54lrykB56c4i3j8VqB6hv3Bzu9TImHg/viewform?usp=sf_link)

Se recolectó lo siguiente:
- [[BRT027-Percepción estudiantes.csv]]
- [[BRT028-A01.pdf]]
- [[BRT029-A02.pdf]]
- [[BRT030-A03.pdf]]
- [[BRT031-A04.pdf]]
- [[BRT032-A05.pdf]]
- [[BRT033-A11.pdf]]
- [[BRT034-A12.pdf]]
- [[BRT035-A14.pdf]]
- [[BRT036-B06.pdf]]
- [[BRT037-B07.pdf]]
- [[BRT038-B08.pdf]]
- [[BRT039-B09.pdf]]
- [[BRT040-B10.pdf]]
- [[BRT041-B13.pdf]]
- [[BRT042-B15.pdf]]

### Participantes 
- Carlos Anchundia
- Efraín Fonseca
- [[Experimentadores no iniciados]]

### Novedades
- [[NVD003-No se ha considerado factores y restricciones adicionales durante la ejecución]]
- [[NVD006-No se exigió que se utilice la herramienta de soporte de forma obligatoria]]
- [[NVD004-Problemas para acceder al materia de soporte - libro]]
