---
tipo_novedad: incidente
nivel_impacto: 🟨 
fecha: 2022-06-14
tipo_amenaza: [conclusion]
---

### Problema
Alumnos que han faltado a sesiones de capacitación
- Mortensen Eduardo (de junio)
- Andrade Mateo (de junio)
- Pérez Marco (21 y 30 de junio)
- Venegas Camila (23 de junio)

### Impacto
- La calidad de la elaboración de la planificación puede verse comprometida.

### Contingencia
- Se desarrollan actividades grupales a manera de tareas
- En cada tarea se realiza un resumen sobre la sesión anterior
