#detalle-actividad

- ### Actividad

Evaluación oral sobre los conocimientos

### Resultados esperados

* Verificar la asimilación de conocimientos y aplacar dudas y ambigüedades sobre la reproducibilidad

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

- [[Revision_de_conceptos_de_repoducibilidad.pdf]]
- [[Revision_de_conceptos_de_repoducibilidad.xlsx]]

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- Ninguna