#detalle-actividad

### Actividad
1. Revisar y contactar a un grupo de [[Experimentadores expertos#Lista de candidatos]].
1. Enviar por correo la [[Invitación de participación]]

### Resultados esperados
* Contar con la participación de los expertos para evaluar los documentos generados por los [[Experimentadores no iniciados]]

### Locación
- Fuera de línea

### Material
- [[Invitación de participación]]

### Participantes
+ Carlos Anchundia
+ Efraín Fonseca

### Novedades
+ Ninguno