#detalle-actividad

### Actividad

Evaluación oral sobre los conocimientos

### Resultados esperados

* Verificar la asimilación de conocimientos 
* Aplacar dudas y ambigüedades sobre la planificación de experimentos

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

- [[BRT001-Revision_de_conceptos_sobre_experimentos.pdf]]
- [[BRT002-Revision_de_conceptos_sobre_experimentos.xlsx]]

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- Ninguna