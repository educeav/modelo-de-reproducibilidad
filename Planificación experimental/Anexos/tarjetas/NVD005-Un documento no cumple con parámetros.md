---
tipo_novedad: [incidente]
nivel_impacto: 🟥
fecha: 2022-07-25
tipo_amenaza: [interna]
---

### Problema
Se revisó cada documento creado por los sujetos. Se detectó que el documento [[BRT034-A12.pdf|A12]] no ha sido realizado de manera consciente y se basó en la literatura de un artículo enfocado a una problemática similar a la del enunciado. Durante la sesión del taller se recalcó que se puede utilizar bibliografía adicional, enfatizando que el objetivo es hacer una planificación y no un artículo. El alumno en cuestión fue el único que no ejecutó adecuadamente el trabajo.

### Impacto
- El documento podría provocar una interferencia para los análisis de resultados

### Contingencia
- Se remueve el documento del proceso de análisis. Esto no afecta al equilibrio de los grupos de análisis.