#detalle-actividad

### Actividad

Tarea en la que deben evaluar dos artículos en relación con la planificación y reproducibilidad.

> [!note]
> Esta tarea tiene como propósito realizar la evaluación de la planificación de experimentos. 
> 
> Las actividades específicas de esta tarea se indican a continuación.  
> 
> 1.  Cada grupo debe desarrollar una matriz de evaluación que se enfoque en la planificación y reproducibilidad de un experimento con base en los conocimientos adquiridos en las clases de experimentación (30 minutos).
> 2.  Aplicar la matriz de evaluación para el análisis de dos experimentos, cuyos reportes estarán disponibles al finalizar el paso anterior (1h30). 
> Notas: 
> -   La matriz debe contener elementos de evaluación considerados básicos para la planificación y la reproducibilidad. Considere como ejemplo el proceso empleado para implementar la matriz de evaluación desarrollada para el survey.

Artículos a evaluar:
- [[MAT004-Artículo1.pdf]]
- [[MAT005-Artículo2.pdf]]

### Resultados esperados

- Que los participantes puedan identificar elementos importantes de un experimento que debieron ser planificados y considerados para la reproducibilidad
- Que los participantes puedan diferenciar la calidad de un artículo en términos de reproducibilidad
- Confirmar si los criterios están consolidados
- Descubrir otros elementos o componentes importantes

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

1. [[BRT021-Tarea 10 - Evaluación de la planificación de experimentos.pdf]]
1. [[BRT022-TAREA10 - GRUPO2.xlsx.pdf]]
1. [[BRT023-TAREA10.pdf]]
1. [[BRT024-TAREA10-Evaluación de la planificación_Grupo4.pdf]]

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- Es difícil evitar que los alumnos quieran mejorar sus matrices mientras realizan la revisión del artículo y descubren más puntos de interés
- Los participantes tienden a confundir:
	- la planificación como etapa
	- que el alcance de la planificación incluye toda la ejecución hasta la finalización del proyecto e investigación.
- Se tuvo que dar consejos sobre como equilibrar el esfuerzo para analizar los documentos