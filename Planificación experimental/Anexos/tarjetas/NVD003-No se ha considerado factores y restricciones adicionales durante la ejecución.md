---
tipo_novedad: [lección]
nivel_impacto: 🟨
fecha: 2022-07-05
tipo_amenaza: [interna]
---

### Problema
La ejecución del taller se pudo llevar a cabo de forma presencial. De esta manera se puede controlar de mejor manera situaciones como el acceso a material adicional y solventar dudas que se pueden tener los estudiantes. 

Los estudiantes pueden estar afectados por la presión de las actividades, el tiempo.

### Impacto
- Posiblemente el alcance de los documentos posiblemente no pueda ser alcanzado y posiblemente se obtenga un mayor detalle de las fases tempranas de la planificación que de las fases tardías

### Contingencia
- Se plantea que los estudiantes traten de abarcar todo el proceso y conforme avance al tiempo incorporen mayor detalle.