#detalle-actividad

### Actividad

Realizar una revisión sobre la matriz de evaluación y los esquemas de revisión que aplicaron.

### Resultados esperados

- Que los participantes puedan comparar sus matrices y definir qué criterios usaron para elaborarla y como ejecutar la evaluación.

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

[[BRT025-Evaluacion_de_articulos-1656622345531.xlsx]]
[[BRT026-Evaluacion_de_articulos-1656622352217.pdf]]

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- Comentarios comunes
	- Un grupo dice que la reproducibilidad fue más difícil que evaluar (G3)
	- Es difícil caracterizar la reproducibilidad como un atributo (G4)
	- El lenguaje usado permite evaluar mejor los artículos (G2)
	- Las secciones permiten evaluar mejor los artículos (G1)
- [[NVD001-Estudiantes ausentes en actividades de inducción]]

