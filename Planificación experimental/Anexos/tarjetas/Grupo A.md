### Herramienta a usar
Modelo

### Material proporcionado
- [[MAT008-Instrucciones grupo A.pdf]]
- link::[MAT009-Modelo de reproducibilidad](https://coggle.it/diagram/YUSyW3-v61dHR6wZ/t/inicio-%F0%9F%8F%81-formular-un-experimento/23aa89db920bea7171eb4220e818cc4bf237d011474a90b20b1fad6a384a2e41) 

### Participantes
- [x] ==Eduardo Antonio Mortensen Franco==
- [x] ==Shirley Stefania Otuna Rojano==
- [x] Marco Santiago Perez Jacome
- [x] Erick German Riascos Moreno
- [x] Jerico Benjamin Ruiz Mafla
- [x] Maycol Estalin Tituaña Tupiza
- [x] Camila Vanessa Venegas Torres
- [x] Christopher Daniel Yepez Chandi