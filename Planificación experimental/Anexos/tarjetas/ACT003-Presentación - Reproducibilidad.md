#detalle-actividad

### Actividad

Clase magistral sobre reproducibilidad que tratará los temas:
1. El contexto y la importancia de la reproducibilidad
2. Las afectaciones de la reproducibilidad
3. El enfoque del diseño e impacto
	1. Proyecto
	2. Comunicación
	3. Colaboración
4. Consejos para un proceso reproducible

### Resultados esperados

* Nivelar a los participantes sobre aspectos que mejorarían la reproducibilidad de una investigación experimental

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

- link::[MAT003-Presentación resproducibilidad](https://coggle.it/diagram/Yq0c1WqIBdEc7NcV/t/reproducibilidad/576f1eab6746814ce4cc8fbd32f8d919d1f11ea4ed290c3edd0185030398832d)

### Participantes

- Carlos Anchundia
- [[Experimentadores no iniciados]]

### Novedades

- [[NVD002-Esta clase se dictó de manera tele presencial a través de Zoom]]