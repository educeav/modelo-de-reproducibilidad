Asunto: Colaboración para revisión de documentos para un Experimento en IS


Estimado/a

Por medio de la presente solicito su colaboración en la revisión de dos planificaciones de experimentos. 

Su participación tiene como finalidad evaluar los criterios considerados por "autores inexpertos" al momento de planificar un experimento en ingeniería de software. Su visión será de mucha utilidad para determinar las brechas que existen entre los distintos niveles de experiencia (inexperto vs. experto).

Para este proceso se solicita que realice las siguientes tareas hasta el día **30 de julio del 2022**:
1. Llenar la encuesta sobre "Aspectos de una investigación" (Formulario azul) (2 min)
2. Realizar una lectura minuciosa los documentos adjuntos (5 min cada uno)
3. Llenar un formulario de "Revisión de planificación de investigación" (Formulario verde) por cada documento  (10 min cada uno)

Agradecemos de antemano su colaboración.

Atentamente,

**Carlos Anchundia**
Departamento de Informática y Ciencias de la Computación
Escuela Politécnica Nacional
Quito - Ecuador
