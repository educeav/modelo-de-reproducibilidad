#detalle-actividad

### Actividad

Tarea grupal sobre la planificación experimental sobre un ejercicio tratado en clase.

>[!note]
>Generar un documento con la planificación de un experimento que permita evaluar si una PC o MAC son mejores para las tareas de desarrollo de software.
>
>Se debe tomar en consideración los siguientes aspectos
> - Basarse en los lineamientos de la presentación sobre [[ACT001-Presentación - Procesos de experimentación]]. 
> - Basarse en la conversación sobre el proceso de experimentación
> - No se proveerá ningún formato o plantilla para la escritura de la planificación
> - Se espera que los estudiantes definan cómo estructurar dicho documento a través de sus propios criterios y/o búsqueda.

### Resultados esperados

* Afianzar los conocimientos y discusiones planteadas en clase, aplicándolos en una tarea manual y grupal.

### Locación

Universidad de las Fuerzas Armadas - ESPE

### Material

- [[BRT003-TAREA8-Grupo1-Planificacion.docx]]
- [[BRT004-TAREA8-GRUPO2-PLANIFICACION-EXPERIMENTO.docx]]
- [[BRT006-TAREA8-Planificacion-Experimento-Grupo3.docx]]
- [[BRT005-TAREA8-Planificacion de experimento-Grupo4.docx]]


### Novedades

Se hicieron las siguientes observaciones sobre los trabajos:
- Mejora en la escritura de oraciones y párrafos
- Explicación sobre el alcance de la planificación
	- Los alumnos tienden a no planificar las etapas de ejecución, análisis y empaquetado
- Revisión de los objetivos de planificación
	- Los alumnos tienden a mezclar los objetivos de sus tareas con los del experimento