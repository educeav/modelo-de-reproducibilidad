#install.packages("dplyr")
#install.packages("gridExtra")
#install.packages("cowplot")

library(tidyverse)
library(rstatix)
library(ggpubr)
library(dplyr)
library(ggplot2)
library(gridExtra)
library(cowplot)


#Tema para graficación
estilo <- theme(
  plot.title = element_text(family = "Helvetica", face = "bold", size = (20)),
  legend.title = element_text(colour = "steelblue", face = "bold.italic", family = "Helvetica"),
  legend.text = element_text(face = "italic", colour = "steelblue4", family = "Helvetica"),
  axis.title = element_text(family = "Helvetica", size = (15), colour = "steelblue4"),
  axis.text = element_text(family = "Courier", colour = "cornflowerblue", size = (15))
)

#Definir working directory
setwd("C://Users//edu_c//Dropbox//Rodrigo-Carlos//ExperimentoTesis//Planificación experimental//04 Análisis de datos")

# Carga de archivos
revision <- read.csv(
  "adjuntos//BTR044-Revisión planificación.csv",
  encoding = "UTF-8"
)

#Renombrado de columnas
names(revision) <- c(
  "ts", 
  "Experto", 
  "Documento", 
  "Acuerdo",
  "Problema",
  "Objetivos_preguntas",
  "Contexto",
  "Hipotesis",
  "Parametros",
  "Sujetos",
  "Muestras_seleccion",
  "Diseño",
  "Instrumentos",
  "Metadatos",
  "Protocolos",
  "Ubicacion",
  "Procesos_incidentes",
  "Ubicacion_incidentes",
  "Procesos_analisis",
  "Procesos_empaquetado",
  "Repositorios",
  "Instrucciones",
  "Codificacion_etiquetas",
  "Glosario",
  "Contacto",
  "Recursos_adicionales",
  "Informacion_herramientas",
  "Uso_repositorios",
  "Despliegue_instancia",
  "Comprension",
  "Esfuerzo",
  "Tecnologias",
  "Toma_decisiones",
  "Autodescrita",
  "Transferible",
  "Estandarizado")

#Cambio de etiquetas de LIKERT a valores
revision[revision == "No identificable" | revision == "Totalmente en desacuerdo" ] = 0
revision[revision == "Difícil" | revision == "Algo en desacuerdo"] = 1
revision[revision == "Normal" | revision == "Indeciso"] = 2
revision[revision == "Fácil" | revision == "Algo de acuerdo"] = 3
revision[revision == "Muy fácil" | revision == "Totalmente de acuerdo"] = 4

# revision[revision == "Totalmente en desacuerdo"] = 0
# revision[revision == "Algo en desacuerdo"] = 1
# revision[revision == "Indeciso"] = 2
# revision[revision == "Algo de acuerdo"] = 3
# revision[revision == "Totalmente de acuerdo"] = 4

#Convertir clase de las columnas a numéricas
cols <- c(5:ncol(revision))
revision[,cols] <- apply(revision[,cols], 2, function(x) as.numeric(as.character(x)))

#Promedios por factor de reproducibilidad
revision$ad <- rowMeans(revision[,c(5:14)], na.rm=TRUE)
revision$t <- rowMeans(revision[,c(15:25)], na.rm=TRUE)
revision$e <- rowMeans(revision[,c(26:28)], na.rm=TRUE)


#Agregación de grupos de documentos
revision$Grupo <-
  ifelse(
    startsWith(revision$Documento,"A"),
    "Modelo",
    "Libro"
  )


# revision %>%
#   group_by(Grupo) %>%
#   summarise(
#     "Promedio" = mean(),
#     "Desviación estándar" = sd()
#   )

# summary(revision)
# 
# revision %>%
#   group_by(Grupo) %>%
#   get_summary_stats(Problema, type = "median_iqr")


revision %>%     wilcox_test(ad ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(t ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(e ~ Grupo) %>%    add_significance()

#Pruebas de hipótesis
revision %>%     wilcox_test(Problema ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Objetivos_preguntas ~ Grupo) %>%    add_sign
revision %>%     wilcox_test(Contexto ~ Grupo) %>%    add_sig
revision %>%     wilcox_test(Hipotesis ~ Grupo) %>%    add_signif
revision %>%     wilcox_test(Parametros ~ Grupo) %>%    add_sig
revision %>%     wilcox_test(Sujetos ~ Grupo) %>%    add_significanc
revision %>%     wilcox_test(Muestras_seleccion ~ Grupo) %>%    add_significan
revision %>%     wilcox_test(Diseño ~ Grupo) %>%    add_s
revision %>%     wilcox_test(Instrumentos ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Metadatos ~ Grupo) %>%    add_significance()

revision %>%     wilcox_test(Protocolos ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Ubicacion ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Procesos_incidentes ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Ubicacion_incidentes ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Procesos_analisis ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Procesos_empaquetado ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Repositorios ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Instrucciones ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Codificacion_etiquetas ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Glosario ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Contacto ~ Grupo) %>%    add_significance()

revision %>%     wilcox_test(Recursos_adicionales ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Informacion_herramientas ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Uso_repositorios ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Despliegue_instancia ~ Grupo) %>%    add_significance()

revision %>%     wilcox_test(Comprension ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Esfuerzo ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Tecnologias ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Toma_decisiones ~ Grupo) %>%    add_significance()

revision %>%     wilcox_test(Autodescrita ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Transferible ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Estandarizado ~ Grupo) %>%    add_significance()

# revision %>%
#   group_by(Grupo) %>%
#   summarise(mean(Problema), median(Problema), sd(Problema), sum(Problema))



# hist(subset(revision, Grupo == "Modelo")$Objetivos_preguntas,
#      main = "Modelo",
#      xlab = "dd")
# hist(subset(revision, Grupo == "Libro")$Problema,
#      main = "Libro",
#      xlab = "dd")
# 
# plot(revision$Objetivos_preguntas, pch = 19,col= factor(revision$Grupo))


# revision %>% 
#   ggplot(mapping = aes(x = Grupo, 
#                        y = Uso_repositorios, 
#                        color = Grupo,
#                        fill = Grupo)) + 
#   #geom_point(size = 3) + 
#   geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
#   labs(title = "Identificación de problema", y = "Nivel de facilidad", x = "Herramienta") +
#   estilo




# table(revision$Grupo)
grupo <- unique(revision$Grupo)
cs <- data.frame(tipo = character(), ad = numeric(), t = numeric(), e = numeric())
for (i in 1:2) {
  cs <- rbind(cs, list(
    tipo = grupo[i], 
    ad = mean(c(
      mean(subset(revision, Grupo==grupo[i])$Problema),
      mean(subset(revision, Grupo==grupo[i])$Objetivos_preguntas),
      mean(subset(revision, Grupo==grupo[i])$Contexto),
      mean(subset(revision, Grupo==grupo[i])$Hipotesis),
      mean(subset(revision, Grupo==grupo[i])$Parametros),
      mean(subset(revision, Grupo==grupo[i])$Sujetos),
      mean(subset(revision, Grupo==grupo[i])$Muestras_seleccion),
      mean(subset(revision, Grupo==grupo[i])$Diseño),
      mean(subset(revision, Grupo==grupo[i])$Instrumentos),
      mean(subset(revision, Grupo==grupo[i])$Metadatos)
    )),
    t = mean(c(
      mean(subset(revision, Grupo==grupo[i])$Protocolos),
      mean(subset(revision, Grupo==grupo[i])$Ubicacion),
      mean(subset(revision, Grupo==grupo[i])$Procesos_incidentes),
      mean(subset(revision, Grupo==grupo[i])$Ubicacion_incidentes),
      mean(subset(revision, Grupo==grupo[i])$Procesos_analisis),
      mean(subset(revision, Grupo==grupo[i])$Procesos_empaquetado),
      mean(subset(revision, Grupo==grupo[i])$Repositorios),
      mean(subset(revision, Grupo==grupo[i])$Instrucciones),
      mean(subset(revision, Grupo==grupo[i])$Codificacion_etiquetas),
      mean(subset(revision, Grupo==grupo[i])$Glosario),
      mean(subset(revision, Grupo==grupo[i])$Contacto)
    )),
    e = mean(c(
      mean(subset(revision, Grupo==grupo[i])$Recursos_adicionales),
      mean(subset(revision, Grupo==grupo[i])$Informacion_herramientas),
      mean(subset(revision, Grupo==grupo[i])$Uso_repositorios),
      mean(subset(revision, Grupo==grupo[i])$Despliegue_instancia)
    ))
  ), stringsAsFactors = FALSE)
}

cs
  
g1 <- revision %>% 
  ggplot(mapping = aes(x = Grupo, 
                       y = ad, 
                       color = Grupo,
                       fill = Grupo)) + 
  #geom_point(size = 3) + 
  geom_boxplot(aes(fill = Grupo), position = position_dodge(0.5), alpha = 0.05) +
  geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
  labs(title = "Auto descripción", y = "Nivel de identificación", x = "") +
  ylim(0,5) +
  estilo

g2 <- revision %>% 
  ggplot(mapping = aes(x = Grupo, 
                       y = t, 
                       color = Grupo,
                       fill = Grupo)) + 
  #geom_point(size = 3) + 
  geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
  geom_boxplot(aes(fill = Grupo), position = position_dodge(0.9), alpha = 0.1) +
  labs(title = "Transferibilidad",y = "Nivel de identificación", x = "") +
  ylim(0,5) +
  estilo

g3 <- revision %>% 
  ggplot(mapping = aes(x = Grupo, 
                       y = e, 
                       color = Grupo,
                       fill = Grupo)) + 
  #geom_point(size = 3) + 
  geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
  geom_boxplot(aes(fill = Grupo), position = position_dodge(0.9), alpha = 0.1) +
  labs(title = "Estandarización",y = "Nivel de identificación", x = "") +
  ylim(0,5) +
  estilo

plot_grid(g1, g2, g3 , labels=c("A","B","C"), ncol = 3, nrow = 1)



revision %>%     wilcox_test(Procesos_empaquetado ~ Grupo) %>%    add_significance()
revision %>%     wilcox_test(Uso_repositorios ~ Grupo) %>%    add_significance()

g4 <- revision %>% 
  ggplot(mapping = aes(x = Grupo, 
                       y = Procesos_empaquetado, 
                       color = Grupo,
                       fill = Grupo)) + 
  #geom_point(size = 3) + 
  geom_boxplot(aes(fill = Grupo), position = position_dodge(0.5), alpha = 0.05) +
  geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
  labs(title = "Empaquetado", y = "Nivel de identificación", x = "") +
  ylim(0,5) +
  estilo

g5 <- revision %>% 
  ggplot(mapping = aes(x = Grupo, 
                       y = Uso_repositorios, 
                       color = Grupo,
                       fill = Grupo)) + 
  #geom_point(size = 3) + 
  geom_jitter(width = 0.1, size = 4, alpha = 0.6) +
  geom_boxplot(aes(fill = Grupo), position = position_dodge(0.9), alpha = 0.1) +
  labs(title = "Repositorios",y = "Nivel de identificación", x = "") +
  ylim(0,5) +
  estilo

plot_grid(g4, g5, labels=c("A","B"), ncol = 2, nrow = 1)



# revision %>%     wilcox_test(ad ~ Grupo) %>%    add_significance()



# library(reshape2)
# gg <- melt(subset(revision[,5:14], Grupo=="Modelo"),id="Problema",value.name="Freq", variable.name="Tipo")
# 
# ggplot(gg, aes(x=Problema, y=Freq, fill=Tipo))+
#   geom_bar(stat="identity")+
#   facet_grid(Tipo~.)





#, p.adjust.method = "holm", paired = FALSE

# wt <- data.frame("")
# 
# resultado = list()
# cols = colnames(revision[5:37])
# for (i in (1:32)) {
#   formula = as.formula( paste(cols[i], cols[33], sep="~") )
#   resultado[[i]] <- wilcox_test(data=revision, formula=formula)
# }
# 
# class(resultado)
# wt[nrow(wt) + 1,]



# cs <- data.frame(tipo = character(), ad = numeric(), t = numeric(), e = numeric())
# cs <- rbind(cs, list(
#   tipo = "Indirecta", 
#   ad = mean(c(
#       mean(expertos$AD_estructura),
#       mean(expertos$AD_navegabilidad),
#       mean(expertos$AD_estandarización)
#     )),
#   t = mean(c(
#       mean(expertos$T_canales),
#       mean(expertos$T_repositorios), 
#       mean(expertos$T_gestión_conocimiento),
#       mean(expertos$T_reportes)
#     )),
#   e = mean(c(
#       mean(expertos$E_revistas),
#       mean(expertos$E_plataformas),
#       mean(expertos$E_infraestructuras),
#       mean(expertos$E_arquitecturas)
#     ))
#   ), stringsAsFactors = FALSE)
# 
# cs <- rbind(cs, list(
#     tipo = "Directa", 
#     ad = mean(expertos$Relevancia_AD),
#     t = mean(expertos$Relevancia_T),
#     e = mean(expertos$Relevancia_E)
#   ), stringsAsFactors = FALSE)
# 
# 
# boxplot(cs$ad , cs$t, cs$e)
# 
# plot(cs$ad, type = "o", col = 2, xlab = "Percepción de relevancia", xaxt = "n", ylab= "Calificación", ylim = c(2, 4), main = "Factores de reproducibilidad")
# lines(cs$t, type = "o", col = 3)
# lines(cs$e, type = "o", col = 4)
# axis(1, 1:nrow(cs), cs$tipo)
# legend("topleft", c("Autodesc.", "Transfer.", "Estand."), lty = 1, col = 2:4)
# 
# 
# summary(expertos)



# plot(expertos$AD_estructura, type = "l",col=2, ylim=c(0,6))
# lines(expertos$AD_navegabilidad, col=3)
# lines(expertos$AD_estandarización,col=4)

# estudiantes %>%
#   group_by(
#     herramienta, 
#     ayuda = aporta_ayuda, 
#     mejora = seguridad_con - seguridad_sin
#   ) %>%
#   summarise(
#     cantidad = n(), 
#     promedio_utilidad = mean(nivel_utilidad)
#   )

# estudiantes %>%
#   group_by(
#     Herramienta,
#     #ayuda = aporta_ayuda,
#   ) %>%
#   summarise(
#     n = n(), 
#     Utilidad = mean(Nivel_utilidad),
#     Seguridad_sin = mean(Seguridad_sin),
#     Seguridad_con = mean(Seguridad_con),
#     Diferencia_seg = Seguridad_con - Seguridad_sin
#   )

# boxplot(
#   estudiantes$nivel_utilidad ~ estudiantes$herramienta
# )

# estudiantes %>%
#   group_by(
#     Herramienta,
#   ) %>%
#   summarise(
#     "X utilidad" = mean(Nivel_utilidad),
#     "Desviación estándar" = sd(Nivel_utilidad)
#   )
# 
# ggplot(estudiantes, aes(x = Nivel_utilidad, color = Herramienta, fill = Herramienta)) +
#   geom_bar(width = .5, position = position_dodge(0.7), alpha = 0.25) +
#   theme(legend.position = "top") +
#   ggtitle("Histograma") +
#   xlab("Nivel de utilidad") +
#   ylab("Conteo")
