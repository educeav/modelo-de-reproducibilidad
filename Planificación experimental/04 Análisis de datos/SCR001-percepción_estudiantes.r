#install.packages("dplyr")
#install.packages("tidyverse")
library(tidyverse)
library(rstatix)
library(ggpubr)
library(dplyr)
library(ggplot2)

#Definir working directory
setwd("C://Users//edu_c//Dropbox//Rodrigo-Carlos//ExperimentoTesis//Planificación experimental//04 Análisis de datos")

#Carga de archivos
estudiantes <- read.csv(
  "adjuntos//BRT027-Percepción estudiantes.csv",
  encoding = "UTF-8"
)

#Renombrado de columnas
names(estudiantes) <- c(
  "ts", 
  "Autor", 
  "Herramienta", 
  "Seguridad_sin",
  "Seguridad_con", 
  "Nivel_utilidad", 
  "Aporta_ayuda", 
  "Descripción_ayuda", 
  "Aporta_dificultad", 
  "Descripción_dificultad")

#Corrección de tipo de herramienta usada
estudiantes[which(estudiantes$Autor == "Mateo Andrade"), 3] = "Libro" 

# estudiantes %>%
#   group_by(
#     herramienta, 
#     ayuda = aporta_ayuda, 
#     mejora = seguridad_con - seguridad_sin
#     ) %>%
#   summarise(
#     cantidad = n(), 
#     promedio_utilidad = mean(nivel_utilidad)
#     )

estudiantes %>%
  group_by(Herramienta) %>%
  summarise(
    n = n(), 
    Utilidad = mean(Nivel_utilidad),
    Seguridad_sin = mean(Seguridad_sin),
    Seguridad_con = mean(Seguridad_con),
    Diferencia_seg = Seguridad_con - Seguridad_sin
  )

# boxplot(
#   estudiantes$nivel_utilidad ~ estudiantes$herramienta
# )

# estudiantes %>%
#   group_by(
#     Herramienta,
#   ) %>%
#   summarise(
#     "X utilidad" = mean(Nivel_utilidad), 
#     "Desviación estándar" = sd(Nivel_utilidad)
#   )

# ggplot(estudiantes, aes(x = Nivel_utilidad, color = Herramienta, fill = Herramienta)) + 
#   geom_bar(width = .5, position = position_dodge(0.7), alpha = 0.25) +
#   theme(legend.position = "top") + 
#   ggtitle("Histograma") +
#   xlab("Nivel de utilidad") +
#   ylab("Conteo")



estudiantes %>%
  group_by(Herramienta) %>%
  get_summary_stats(Nivel_utilidad, type = "median_iqr")
  #get_summary_stats(Nivel_utilidad, type = "mean_sd")


# ANOVA NO
# estudiantes %>% 
#   anova_test(Nivel_utilidad ~ Herramienta)

ggboxplot(
  estudiantes, x = "Herramienta", y = "Nivel_utilidad", 
  color = "Herramienta", shape = "Herramienta", 
  ylab = "Utilidad percibida", xlab = "Herramientas", add = "jitter")

# estudiantes %>%
#   wilcox_test(Nivel_utilidad ~ Herramienta) %>%
#   add_significance()

#Probar norlamidad
# hist(subset(estudiantes, Herramienta == "Modelo")$Nivel_utilidad,
#      main = "cc",
#      xlab = "dd")
# hist(subset(estudiantes, Herramienta == "Libro")$Nivel_utilidad,
#      main = "cc",
#      xlab = "dd")
# shapiro.test(subset(estudiantes, Herramienta == "Modelo")$Nivel_utilidad)
# shapiro.test(subset(estudiantes, Herramienta == "Libro")$Nivel_utilidad)

#ggqqplot(estudiantes, "Nivel_utilidad", facet.by = "Herramienta")
