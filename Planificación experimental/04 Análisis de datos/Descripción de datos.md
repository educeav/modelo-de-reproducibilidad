## Protocolo de validación 

<table cellpadding="5" class="table">
	<tr>
		<th><p>Etapa</p></th>
		<th><p>Actividades</p></th>
	</tr>	
	
	<tr>
		<td style="vertical-align:top"><p>Análisis</p></td>
		<td>
			<ul>
				<li>Validar la recepción de los documentos y la participación de los expertos.</li>
				<li>Validar la consistencia de datos.</li>
				<li>Validar la exclusión de documentos que puedan causar interferencia.</li>
				<li>Validar la recolectar la revisión de expertos a través de una plataforma Google Forms.</li>
				<li>Validar la creación un script en R para automarizar la transformación de datos .</li>
			</ul>
		</td>
	</tr>
</table>

## Datos a analizar
Para el análisis de datos se ha considerado los siguientes aspectos:
### Percepción de estudiantes
Los estudiantes, experimentadores no iniciados, al inicio del proceso contaban con un grado básico de conocimiento sobre métodos de investigación, principalmente enfocados a las encuestas. Durante el proceso de inducción, los estudiantes fueron instruidos en los conceptos de experimentación y reproducibilidad. 

Se hizo hincapié sobre lo fundamental que es considerar aspectos de calidad desde la concepción de un experimento, por lo que se planteó considerar a la reproducibilidad desde las etapas de planificación. Esto permitirá a los estudiantes planificar un proceso de experimentación sin llegar a instanciarlo. Sin embargo, consideramos que la planificación puede plasmar muchos elementos importantes que deberán seguirse durante la ejecución y que podrán influenciar varios aspectos de reproducibilidad.

Los estudiantes están expuestos a mucha literatura que se enfoca principalmente en la conceptualización e instrucción sobre un tema. Es por esto que consideramos interesante analizar cómo un estudiante que está acostumbrado a una herramienta de soporte tradicional, como un libro, puede sentirse al enfrentar otro esquema de soporte totalmente distinto.

Como resultado de la actividad [[ACT008-Ejecución de taller - Grupo A y B]] se recopiló el archivo [[BRT027-Percepción estudiantes.csv]] que incluye los datos sobre la ejecución de la planificación solicitada desde la perspectiva de los experimentadores "no iniciados". En este archivo se destacan datos sobre:
- Su percepción sobre el uso de las distintas herramientas: Modelo (propuesta) y Libro (material tradicional).
- Su nivel de seguridad para realizar la tarea, sin el uso de herramientas de soporte proporcionada.
- Su nivel de seguridad para ejecutar la tarea, con el empleo de la herramienta de soporte proporcionada.

### Percepción de expertos
Los expertos, quienes evaluarán los documentos sobre planificación de experimentos, cuentan con experiencia en la ejecución de procesos de investigación experimental. Ellos son ajenos a las propuestas y hallazgos que se han hecho sobre el modelo de reproducibilidad propuesto.

Ellos evaluarán varios aspectos de la reproducibilidad percibida en los trabajos producidos por los estudiantes. Por lo tanto, hemos considerado conocer la apreciación de los evaluadores respecto a diversos factores que propician a la reproducibilidad.

Los expertos parecen ser conscientes de la importancia de la reproducibilidad; sin embargo, aún existen problemas al tratar de reproducir o replicar trabajos de investigación experimentales. Por eso que consideramos interesante analizar sus creencias respecto a la reproducibilidad respecto a las actividades que suelen ejecutar durante el desarrollo de sus actividades de investigación. De igual manera, estas mismas actividades son las que deben evaluar en los documentos generados por los estudiantes.

Como resultado de la actividad [[ACT009-Selección de expertos y envío de instrumentos generados]] se recopiló el archivo [[Aspectos de una investigación.csv]] que incluye los criterios de los expertos sobre la reproducibilidad. En este archivo se destacan datos sobre:
- Su opinión sobre aspectos que propician la auto descripción de una investigación.
- Su opinión sobre aspectos que propician la transferibilidad de una investigación.
- Su opinión sobre aspectos que propician la estandarización de una investigación.
- Su opinión sobre los tres factores que propician la reproducibilidad de una investigación.

### Resultados de los tratamientos
Ya que los expertos evaluarán las planificaciones presentadas por los estudiantes, se recopiló un formulario para recopilar diversos aspectos que permiten la [[Auto descripción]], [[Transferibilidad]] y [[Estandarización]] de una investigación.

Como resultado de la actividad [[ACT010-Recolección de instrumentos evaluados]] se recopiló el archivo [[BTR044-Revisión planificación.csv]] que incluye la revisión de cada documento. En este archivo se cubren datos sobre las variables presentadas en [[Hipótesis y variables#Variables]], principalmente evaluando la presencia o consideración explícita de los siguientes elementos:
- Datos relevantes sobre la investigación
- Procedimientos relevantes sobre la investigación
- Descripción de artefactos relevantes sobre la investigación
- Descripción de repositorios de la investigación
- Descripción de tecnologías y herramientas relevantes sobre la investigación
- Uso de tecnologías y herramientas estandarizadas en la investigación
