## La utilidad de la herramienta

Se genera el siguiente script de R: [[percepción_estudiantes.r]]

En la siguiente tabla se presenta los promedios de la utilidad de la herramienta percibida por los estudiantes. También se muestra la seguridad del estudiante para poder cumplir la tarea solicitada sin el uso de ninguna herramienta, así como la seguridad con el uso de la herramienta.

```R
Herramienta     n Utilidad Seguridad_sin Seguridad_con Diferencia_seg
  <chr>       <int>    <dbl>         <dbl>         <dbl>          <dbl>
1 Libro           7     4.43          3.43          4             0.571
2 Modelo          8     4.25          3.75          3.88          0.125
```

La herramienta tradicional, los libros, es percibida como de mayor utilidad. Por otro lado, el modelo presentado, parece ser percibida como menos útil para el cumplimiento de la tarea solicitada. Esto puede ser posible debido a la expectativa de enfrentarse a la utilización de una herramienta nunca antes vista.

Cabe destacar que los estudiantes que usaron el modelo tuvieron al rededor de 5 minutos para examinar la estructura de las herramientas provistas.

Aunque el modelo es percibido con una utilidad inferior, este criterio es menos disperso que la opinión sobre el libro.
```r
  Herramienta variable           n  mean    sd
  <chr>       <chr>          <dbl> <dbl> <dbl>
1 Libro       Nivel_utilidad     7  4.43 0.787
2 Modelo      Nivel_utilidad     8  4.25 0.707




  Herramienta variable           n median   iqr
  <chr>       <chr>          <dbl>  <dbl> <dbl>
1 Libro       Nivel_utilidad     7      5     1
2 Modelo      Nivel_utilidad     8      4     1
```

![[Pasted image 20220730181850.png|300]]

Realizando un análisis ANOVA, no existe una diferencia significativa en la utilidad percibida sobre el uso de las diferentes herramientas. Realizando un análisis Wilcox test, no existe una diferencia significativa en la utilidad percibida sobre el uso de las diferentes herramientas.

```R
ANOVA Table (type II tests)

       Effect DFn DFd     F     p p<.05   ges
1 Herramienta   1  13 0.215 0.651       0.016


  .y.            group1 group2    n1    n2 statistic     p p.signif
  <chr>          <chr>  <chr>  <int> <int>     <dbl> <dbl> <chr>   
1 Nivel_utilidad Libro  Modelo     7     8      32.5 0.613 ns
```

![[Pasted image 20220801214641.png|400]]

![[Pasted image 20220802155349.png|400]]

## La reproducibilidad



## La aplicación del modelo