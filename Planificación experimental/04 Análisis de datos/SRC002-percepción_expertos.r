#install.packages("dplyr")
library(ggplot2)
library(dplyr)

#Definir working directory
setwd("C://Users//edu_c//Dropbox//Rodrigo-Carlos//ExperimentoTesis//Planificación experimental//04 Análisis de datos")

# Carga de archivos
expertos <- read.csv(
  "adjuntos//BRT043-Aspectos investigación.csv",
  encoding = "UTF-8"
)

#Renombrado de columnas
names(expertos) <- c(
  "ts", 
  "Experto", 
  "AD_estructura", 
  "AD_navegabilidad",
  "AD_estandarización", 
  "AD_otras", 
  "T_canales", 
  "T_repositorios", 
  "T_gestión_conocimiento",
  "T_reportes",
  "T_otras",
  "E_revistas",
  "E_plataformas",
  "E_infraestructuras",
  "E_arquitecturas",
  "E_otras",
  "Relevancia_AD",
  "Relevancia_T",
  "Relevancia_E",
  "Reproducibilidad_personal",
  "Reproducibilidad_grupo",
  "Reproducibilidad_otros")

#Cambio de etiquetas de LIKERT a valores
expertos[expertos == "Nunca"] = 0
expertos[expertos == "Raramente"] = 1
expertos[expertos == "A veces"] = 2
expertos[expertos == "A menudo"] = 3
expertos[expertos == "Siempre"] = 4

expertos[expertos == "Nada relevante"] = 0
expertos[expertos == "Poco relevante"] = 1
expertos[expertos == "Algo relevante"] = 2
expertos[expertos == "Relevante"] = 3
expertos[expertos == "Muy relevante"] = 4

expertos[expertos == "Muy pobre"] = 0
expertos[expertos == "Pobre"] = 1
expertos[expertos == "Razonable"] = 2
expertos[expertos == "Buena"] = 3
expertos[expertos == "Excelente"] = 4

#Convertir clase de las columnas a numéricas
cols <- c(3:5,7:10,12:15,17:22)
expertos[,cols] <- apply(expertos[,cols], 2, function(x) as.numeric(as.character(x)))

#revisión de factores
cs <- data.frame(tipo = character(), ad = numeric(), t = numeric(), e = numeric())
cs <- rbind(cs, list(
  tipo = "Calificación\nindirecta", 
  ad = mean(c(
      mean(expertos$AD_estructura),
      mean(expertos$AD_navegabilidad),
      mean(expertos$AD_estandarización)
    )),
  t = mean(c(
      mean(expertos$T_canales),
      mean(expertos$T_repositorios), 
      mean(expertos$T_gestión_conocimiento),
      mean(expertos$T_reportes)
    )),
  e = mean(c(
      mean(expertos$E_revistas),
      mean(expertos$E_plataformas),
      mean(expertos$E_infraestructuras),
      mean(expertos$E_arquitecturas)
    ))
  ), stringsAsFactors = FALSE)

cs <- rbind(cs, list(
    tipo = "Calificación\ndirecta", 
    ad = mean(expertos$Relevancia_AD),
    t = mean(expertos$Relevancia_T),
    e = mean(expertos$Relevancia_E)
  ), stringsAsFactors = FALSE)

plot(cs$ad, type = "o", col = 2, xlab = "Factores", xaxt = "n", ylab= "Nivel de relevancia", ylim = c(2, 4))
lines(cs$t, type = "o", col = 3, lty = 2)
lines(cs$e, type = "o", col = 4, lty = 3)
axis(1, 1:nrow(cs), cs$tipo)
legend("topleft", c("Auto descripción", "Transferibilidad", "Estandarización"), lty = c(1, 2, 3), col = 2:4)


summary(expertos)

#boxplot(cs$ad , cs$t, cs$e)


# plot(expertos$AD_estructura, type = "l",col=2, ylim=c(0,6))
# lines(expertos$AD_navegabilidad, col=3)
# lines(expertos$AD_estandarización,col=4)

# estudiantes %>%
#   group_by(
#     herramienta, 
#     ayuda = aporta_ayuda, 
#     mejora = seguridad_con - seguridad_sin
#   ) %>%
#   summarise(
#     cantidad = n(), 
#     promedio_utilidad = mean(nivel_utilidad)
#   )

# estudiantes %>%
#   group_by(
#     Herramienta,
#     #ayuda = aporta_ayuda,
#   ) %>%
#   summarise(
#     n = n(), 
#     Utilidad = mean(Nivel_utilidad),
#     Seguridad_sin = mean(Seguridad_sin),
#     Seguridad_con = mean(Seguridad_con),
#     Diferencia_seg = Seguridad_con - Seguridad_sin
#   )

# boxplot(
#   estudiantes$nivel_utilidad ~ estudiantes$herramienta
# )

# estudiantes %>%
#   group_by(
#     Herramienta,
#   ) %>%
#   summarise(
#     "X utilidad" = mean(Nivel_utilidad),
#     "Desviación estándar" = sd(Nivel_utilidad)
#   )
# 
# ggplot(estudiantes, aes(x = Nivel_utilidad, color = Herramienta, fill = Herramienta)) +
#   geom_bar(width = .5, position = position_dodge(0.7), alpha = 0.25) +
#   theme(legend.position = "top") +
#   ggtitle("Histograma") +
#   xlab("Nivel de utilidad") +
#   ylab("Conteo")
