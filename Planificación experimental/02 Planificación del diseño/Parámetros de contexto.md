Los parámetros de este experimento constituyen variables relacionadas con el proceso de creación de un experimento reproducible.

```plantuml
@startmindmap

*[#lightgreen] Salidas
** Nivel de reproducibilidad de experimento
***_ Auto descripción
***_ Transferibilidad
***_ Estandarización

*[#lightblue] Entradas
** Experimento
***_ Inducción sobre experimentación y reproducibilidad 
***_ Experimento con dominio en estrategias de programación
** Participantes
***_ Experimentadores no iniciados para ejecutar el experimento
***_ Experimentadores expertos para analizar los resultados
** Tratamientos
***_ Modelo de reproducibilidad
***_ Literatura base de experimentación

*[#orange] Contexto
** Dominio
***_ Replicación de experimentos en IS
** Integración
***_ Fuera de línea, es decir se formulará solamente la planificación
** Especificidad
***_ Genérica, es decir su interpretación puede ser transferible

@endmindmap
```

##
[^wohlin]: Sacado del libro de Wohlin