## Instrumentos
Los instrumentos utilizados en este experimento caen en dos tipos: archivos generados como material para capacitación o de recopilación de información; y archivos ubicados en línea como formularios de encuestas digitales. A continuación se muestran los instrumentos de la bóveda del experimento y los accesibles en línea.

```dataviewjs
let aguja = "MAT";

dv.table(
	["Artefactos", 
	"Fuente"],
dv.pages()
	.where(p => JSON.stringify(p.file.outlinks).includes(aguja))
	.map(p => 
	[p.file.outlinks.filter(enlace => enlace.path.includes(aguja)), 
	p.file.link])
);
```

```dataviewjs
let aguja = "MAT";

dv.table(
	["Artefactos", 
	"Fuente"],
dv.pages()
	.where(p => p.link)
	.where(p => JSON.stringify(p.link).includes(aguja))
	.map(p => 
	[p.link, 
	p.file.link])
);
```
