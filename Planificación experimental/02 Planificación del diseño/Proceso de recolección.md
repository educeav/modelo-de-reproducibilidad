## Actividades para recolección
### Ejecución del proceso 
Todas las actividades de ejecución están disponibles a través del [[Tablero Kanban]]. Cada actividad deberá tener una descripción y una fecha. Para detallar se ha configurado la creación del detalle a partir de la plantilla [[Detalle de actividad]].

### Registro de novedades
Las novedades deberán se registran en el *detalle de una actividad* bajo la sección Novedades. El registro de una novedad seguirá la plantilla especificada en [[Registro de novedad]]
 
## Protocolos de validación
El proceso de experimentación a lo largo de las etapas será la siguiente:

<table cellpadding="5" class="table">
	<tr>
		<th><p>Etapa</p></th>
		<th><p>Actividades</p></th>
	</tr>		
	<tr>
		<td style="vertical-align:top"><p>Inducción</p></td>
		<td>
			<ol>
				<li>Validar la experiencia de los participantes sobre conceptos de investigación.</li>
				<li>Validar el contenido de las induciones.</li>
				<li>Validar la asistencia participantes para asegurar efectividad de sus conocimientos.</li>
				<li>Validar la adquisición de conocimientos a través de trabajos y actividades en clase.</li>
			</ol>
		</td>
	</tr>	
	<tr>
		<td style="vertical-align:top"><p>Recolección</p></td>
		<td>
			<ol>
				<li>Validar la recolección de los documentos a través de la pataforma institucional.</li>				
				<li>Validar la entrega de las encuestas y del correcto llenado de los datos.</li>
			</ol>
		</td>
	</tr>
	<tr>
		<td style="vertical-align:top"><p>Registro</p></td>
		<td>
			<ol>
				<li>Validar el renombrado de los documentos bajo la nomenclaruta especificada-</li>
				<li>Validar el registro de novedades ocurridas durante la inducción y recolección.</li>
				<li>Validar los documentos recolectados para asegurar el cumplimiento de la actividad.</li>
			</ol>
		</td>
	</tr>
</table>


## Nomenclatura (Actividades, materiales y anexos)
Los artefactos serán codificados de la siguiente manera:
`<TIPO><###>-<breve_descripción>[.<ext>]`
Por ejemplo: 
`MAT001-ComentariosDeAutores.docx`

| TIPO | Significado                                                     |
| ---- | --------------------------------------------------------------- |
| MAT  | Material utilizado para actividad                               |
| BRT  | Artefactos en bruto recabados o generados durante una actividad |
| PRO  | Artefactos procesados o transformados                           |
| SCR  | Scripts o código utilizado para procesar artefactos             |
| RES  | Resultados extraídos                                            |
| ACT  | Actividad                                                       |
| NVD  | Novedad                                                                |
