## Tipo de diseño
Para este experimento se plantean dos etapas: 

### Etapa de intervención

Se han considerado las siguientes características para el experimento:

| Ítem        | Descripción                                                                            |
| ----------- | -------------------------------------------------------------------------------------- |
| Factor      | Tipo de soporte de investigación (Modelo, Libro base)                                  |
| Sujeto      | [[Experimentadores no iniciados]]                                                      |
| Objeto      | Experimento                                                                            |
| Tratamiento | Desarrollo de la planificación de un experimento utilizando una herramienta de soporte |
| Variables   | Auto descripción, Transferibilidad, Estandarización                                    |
| Grupos      | Dos: Grupo A con modelo y Grupo B con libro base                                                                                       |
| Asignación  | Aleatoria                                                                              |

### Etapa de análisis

| Ítem        | Descripción                                                                            |
| ----------- | -------------------------------------------------------------------------------------- |
| Factor      | Tipo de soporte de investigación (Modelo, Libro base)                                  |
| Sujeto      | [[Experimentadores expertos]]                                                      |
| Objeto      | Experimento                                                                            |
| Tratamiento | Desarrollo de la planificación de un experimento utilizando una herramienta de soporte |
| Variables   | Auto descripción, Transferibilidad, Estandarización                                  |
| Asignación  | Aleatoria                                                                                       |
