# Proceso de planificación del diseño
```plantuml
scale 1
(*) --> "[[[Parámetros de contexto|Definición de parámetros de contexto]]]"
--> === F1 ===
=== F1 === --> "[[[Sujetos|Identificación de sujetos]]]" as is
=== F1 === --> "[[[Hipótesis y variables|Definición de hipótesis y variables]]]" as dh
is --> === F2 ===
dh --> === F2 ===
--> "[[[Muestra|Selección de muestra]]]"
--> "[[[Diseño experimental|Selección del diseño experimental]]]"
--> === F3 ===
=== F3 === --> "[[[Proceso de recolección|Definición del \n proceso de \n recolección]]]" as dpr
=== F3 === --> "[[[Técnicas de análisis|Selección de \n técnicas de \n análisis]]]" as sta
=== F3 === --> "[[[Instrumentación|Selección de \n instrumentación]]]" as sin
dpr --> === F4 === 
sta --> === F4 === 
sin --> === F4 ===
--> (*)
```
## Entidades
```plantuml
scale 1.2
class "Contexto" as Contexto
Contexto -right-> Dominio
Contexto o--> Muestra
Contexto o--> Especificidad
Contexto o--> "Tipo de integración"

abstract class Muestra

enum Especificidad {
 GENÉRICA
 DE DOMINIO
}

enum "Tipo de integración" {
 EN LÍNEA
 FUERA DE LÍNEA
}
```

```plantuml
scale 1.2

abstract class Muestra
abstract class Sujeto

Muestra <|--- "Muestra de participante"
Muestra <|--- "Muestra de artefacto"
"Muestra de participante" o--->"*" Persona 
"Muestra de artefacto" o--->"*" Artefacto
Sujeto <|-- Persona
Sujeto <|-- Artefacto
Persona <|-- Estudiante
Persona <|-- Investigador
Persona <|-- Profesional
Estudiante <|-- "No graduado"
Estudiante <|-- Graduado
Artefacto -right-> "Nivel de realidad"
Muestra -> Población
(Muestra, Población) ..> Generalización
Generalización --> Generalidad

enum Generalidad{
LOCAL
ORGANIZACIONAL
REGIONAL
GLOBAL
}

enum "Nivel de realidad"{
JUEGO
MUNDO REAL
}
```




```plantuml
scale 1.2
class Objetivo
class "Atributo de calidad" as at
Objetivo *-->"*" Meta
Meta -right->"1..*" at
Meta -up->"*" Interesado
at -up-> Interesado
(Meta,at) ..> Propósito
Propósito --> "Tipo de propósito"
Meta --> Pregunta
Pregunta --> Hipótesis: formalización
Hipótesis -->"1..*" Métrica
Hipótesis *--> "Hipótesis Nula"
Hipótesis *--> "Hipótesis Alternativa"
Métrica -->"*" Independiente
Métrica -->"*" Dependiente
Independiente --|> Variable
Dependiente --|> Variable

enum "Tipo de propósito"{
INCREMENTAR
DECREMENTAR
BAJAR EL UMBRAL
SUPERAR EL UMBRAL
ENTRE EL UMBRAL
}
```

```plantuml
scale 1.2
left to right direction

abstract class Muestra

Muestra --> Organización
Muestra --> "Método de muestreo"

Organización <|-- Simple
Organización <|-- Estratificada
Organización <|-- Agrupada
Organización <|-- Cuota

"Método de muestreo" <|-- Aleatorio
"Método de muestreo" <|-- Sistemático
"Método de muestreo" <|-- Conveniente
```
```plantuml
scale 1.2

left to right direction
abstract class Intervención

"Diseño experimental" *-->"1..*" Grupo
Grupo *->"1..*" Intervención

Intervención <|-- Tratamiento
Intervención <|-- Observación

(Grupo, Intervención) ..> "Marca de tiempo"

Grupo <|-- "Único"
Grupo <|-- Aleatorio
Grupo <|-- "No equivalente"
Grupo <|-- "Aislado"
```

# Desarrollo
![[Parámetros de contexto]]

![[Sujetos]]

![[Hipótesis y variables]]

![[Muestra]]

![[Diseño experimental]]

![[Proceso de recolección]]

![[Técnicas de análisis]]

![[Instrumentación#Instrumentos]]
