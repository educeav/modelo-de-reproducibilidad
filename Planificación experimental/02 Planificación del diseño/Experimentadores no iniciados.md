## Experimentadores no iniciados (experimento)

El experimentador no iniciado es un candidato ideal para este proceso. Este tipo de participante carece de un posible sesgo procedimental tanto en la práctica de la experimentación, así como del dominio de una temática investigativa. 

Adicionalmente, alguien experimentado tiene un grado de conocimiento tácito sobre los procedimientos y capacidad para determinar elementos de importancia sobre un experimento. De esta manera.

## Características

| Ítem           | Detalle                                          |
| -------------- | ------------------------------------------------ |
| Población      | Estudiantes de ingeniería de software de la ESPE |
| Generalización | Global                                           |
| Muestra        | Estudiantes de Investigación en IS               |
| Sujetos        | Estudiantes de quinto nivel                      |

## Lista de participantes por grupo
![[Grupo A#Participantes]]

![[Grupo B#Participantes]]
