---
persona: experto_participante
---

## Martín Solari
[[Experimentadores expertos]]

País:: Uruguay
Participación:: ✔

martin.solari@ort.edu.uy
martinsolari@gmail.com

Documentos:: [[BRT035-A14.pdf|A14]], [[BRT036-B06.pdf|B06]]