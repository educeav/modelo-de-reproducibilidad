---
persona: experto_participante
---

## Oscar Dieste
[[Experimentadores expertos]]

 País:: España
 Participación:: ✔

oscar.dieste@gmail.com
odieste@fi.upm.es

Documentos:: [[BRT035-A14.pdf|A14]], [[BRT036-B06.pdf|B06]]