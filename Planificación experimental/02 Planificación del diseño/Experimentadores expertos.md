## Experimentadores expertos (análisis)
Por otro lado, un experimentador iniciado es importante para validar la planificación de un experimento. Según Nehm[^nehm2011], un experto es capaz de notar patrones significativos, organizar contenido y reflejar contextos de aplicabilidad; todo esto con un esfuerzo pequeño.

Por esta razón, podría validar la capacidad para identificar si los elementos básicos para una reproducibilidad efectiva están presentes o no en un proceso de planificación.

[^nehm2011]: What Do Experts and Novices "See" in Evolutionary Problems? (2011)

## Características 

| Ítem           | Detalle                                                 |
| -------------- | ------------------------------------------------------- |
| Población      | Investigadores con experiencia en experimentación en IS |
| Generalización | Global                                                  |
| Muestra        | Grupos de investigación                                 |
| Sujetos        | 13 investigadores                                        |

Los expertos deberán contar con la siguientes características:
- Experiencia en experimentos de más de 5 años
- Experiencia en replicaciones
* Formar parte de un grupo de investigación

## Lista de candidatos

```dataview
table Participación, País
from -"Planificación experimental/Anexos/plantillas"
where contains(persona,"experto_participante")
```
