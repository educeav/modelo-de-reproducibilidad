## Hipótesis

[[Objetivo general#Objetivo general]]

> [!FAQ] Pregunta de investigación
> ¿Los factores de auto descripción, transferibilidad y estandarización inciden en la reproducibilidad de una investigación?           

>[!important] Hipótesis nula
>No existe diferencia en la reproducibilidad de un experimento al emplear un *modelo basado en reproducibilidad* o el *proceso de experimentación* cuando se lo planifica o interpreta.

>[!important] Hipótesis alternativa
>Existe una diferencia en la reproducibilidad de un experimento al emplear un *modelo basado en reproducibilidad* o el *proceso de experimentación* cuando se lo planifica o interpreta.

## Métrica
Hemos considerado a la reproducibilidad como un aspecto de calidad. Por lo tanto, hemos planteado tres factores de calidad:

![[Auto descripción]]

![[Transferibilidad]]

![[Estandarización]]

## Variables
```plantuml
@startmindmap
*[#yellow] Reproducibilidad
**[#lightgreen] Auto descripción
*** Número de características relevantes incluidas
*** Número de características relevantes excluidas
*** Número de interacciónes personales requeridas

**[#lightblue] Transferibilidad
*** Tiempo empleado para encontrar información solicitada
*** Cantidad de información "en bruto" disponible
*** Cantidad de información "procesada" disponible
*** Cantidad de artefactos disponibles

**[#orange] Estandarización
*** Tipo de artefacto
****_ Físico
****_ Digital
*** Gestión de conocimiento
****_ Estática 
****_ Dinámica
*** Tipo de plataforma/tecnología de gestión
****_ Cerrada
****_ Abierta
*** Dominio
****_ Específico
****_ General
*** Instanciación
****_ A nivel de elementos
****_ A nivel de experimento
@endmindmap
```

