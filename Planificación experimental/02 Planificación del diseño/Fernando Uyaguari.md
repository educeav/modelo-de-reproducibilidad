---
persona: experto_participante
---

## Fernando Uyaguari
 [[Experimentadores expertos]]
 
 País:: Ecuador
 Participación:: ✔

fuyaguari01@gmail.com
fuyaguari@hotmail.com
fernando.uyaguari@wissen.edu.ec

Documentos:: [[BRT030-A03.pdf|A03]], [[BRT040-B10.pdf|B10]]