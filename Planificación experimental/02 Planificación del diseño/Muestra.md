Para este experimento se contará con la participación de dos segmentos grupales, estudiantes y expertos, cada uno obedeciendo a las siguientes características:

| Experimentadores: |         no iniciados          |                expertos                 |
| -----------------:|:-----------------------------:|:---------------------------------------:|
|     Organización: |           Agrupada            |              Estratificada              |
|         Muestreo: | Conveniente <br> (Quinto nivel) | Sistemática <br> (Por experiencia) |

Los "no iniciados" ejecutarán el experimento y los "expertos" aportarán con el proceso de confirmación de resultados.