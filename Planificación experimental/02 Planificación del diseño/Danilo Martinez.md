---
persona: experto_participante
---

## Danilo Martinez
 [[Experimentadores expertos]]
 
 País:: Ecuador
 Participación:: ✔

mdmartinez@espe.edu.ec
dmartinezes@gmail.com

Documentos::  [[BRT033-A11.pdf|A11]], [[BRT037-B07.pdf|B07]]