# 03 Recolección de datos
```plantuml
scale 1.2
(*) --> "Autorización de recolección"
--> "Motivación de los participantes"
--> "Validaciones de datos"
--> "Reporte de problemas"
--> (*)
```
## Entidades
# Desarrollo

![[Autorización para recolección de datos]]

![[Motivación de los participantes]]

![[Recolección de datos#Arterfactos]]

![[Validación de datos]]

![[Reporte de novedades]]
