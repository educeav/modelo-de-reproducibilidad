El proceso será llevado a cabo a través de la participación de estudiantes y expertos en el tema.

### Estudiantes
La motivación de los estudiantes será a través de una recompensa académica. El experimento es parte de una asignatura por aprobar *investigación en Ingeniería de Software*. Posiblemente, esto garantice un esfuerzo real sobre el éxito de las metas propuestas en los talleres.

### Expertos
Por otro lado, los expertos serán invitados a colaborar del proceso de revisión. Los expertos forman parte de grupos de investigación con los que se ha trabajado con anterioridad. Por lo tanto, existe un grado de compromiso y amistad que podría motivar el reconocimiento de su participación y aporte a los temas de investigación en el área.