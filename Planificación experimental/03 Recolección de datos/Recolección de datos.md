## Arterfactos 
```dataviewjs
let artefactos = dv.pages().file.outlinks.filter(enlace => enlace.path.includes("BRT")).sort(s => [s], ['asc']);
dv.table(
	["Artefactos en bruto recolectados"],
	artefactos.map( enlace => [enlace]));
```
## Artefactos por referencia
```dataviewjs
let aguja = "BRT";

dv.table(
	["Referencia", 
	"Artefactos"],
dv.pages('"Planificación experimental/Anexos/tarjetas"')
	.where(p => JSON.stringify(p.file.outlinks).includes(aguja))
	.map(p => 
	[p.file.link,
	p.file.outlinks.filter(enlace => enlace.path.includes(aguja))])
);
```



